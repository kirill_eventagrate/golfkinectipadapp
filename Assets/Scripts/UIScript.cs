﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SharpOSC;

public class UIScript : MonoBehaviour {

    public GameObject DataPanel;
    public GameObject StartPanel;
    public GameObject SettingsPanel;
    public InputField NameInput;
    public InputField EmailInput;
    public InputField PhoneInput;
    public InputField IPInput;
    public Button SaveBtn;
    public Button StartBtn;
    public Button SettingBtn;
    public Button SaveSettingBtn;
    public Button CloseSettingBtn;

    bool isMessageSend = false;

    OscMessage messageReceived;
    UDPListener listener;
    // Use this for initialization
    void Start () {
        if (!PlayerPrefs.HasKey("IP"))
            PlayerPrefs.SetString("IP", "127.0.0.1");

        IPInput.text = PlayerPrefs.GetString("IP");

        listener = new UDPListener(7000);
        messageReceived = null;

        SaveBtn.onClick.AddListener(() => SaveData());
        StartBtn.onClick.AddListener(() => CheckGameStart());
        SaveSettingBtn.onClick.AddListener(() => SaveSetting());

        SettingBtn.onClick.AddListener(() => ShowHideSettingPanel(true));
        CloseSettingBtn.onClick.AddListener(() => ShowHideSettingPanel(false));
    }
	
	// Update is called once per frame
	void Update () {
        if (isMessageSend)
        {
            messageReceived = (OscMessage)listener.Receive();
            if (messageReceived != null)
            {
                if (messageReceived.Address == "/isGamePlaying")
                {
                    string message = messageReceived.Arguments[0].ToString();
                    Debug.Log(message);
                    /*if (!message)
                    {

                    }*/
                    isMessageSend = false;
                }
            }
        }
	}

    public void SaveData()
    {
        DataPanel.SetActive(false);
        StartPanel.SetActive(true);
        
    }

    public void RunMainApp()
    {
        SendData();

    }

    void ResetData()
    {
        NameInput.text = "";
        EmailInput.text = "";
        PhoneInput.text = "";
        DataPanel.SetActive(true);
        StartPanel.SetActive(false);
    }

    public void SendData()
    {
        Debug.Log(IPInput.text);
        string sendData = NameInput.text + ";" + EmailInput.text + ";" + PhoneInput.text;
        var message = new OscMessage("/newUser", sendData);
        var sender = new UDPSender(IPInput.text, 8000);
        sender.Send(message);
    }

    void ShowHideSettingPanel(bool _val)
    {
        /*DataPanel.SetActive(!_val);
        StartPanel.SetActive(!_val);*/
        SettingsPanel.SetActive(_val);
    }

    void SaveSetting()
    {
        PlayerPrefs.SetString("IP", IPInput.text);
    }

    void CheckGameStart()
    {
        var message = new OscMessage("/isGamePlaying");
        var sender = new UDPSender(IPInput.text, 8000);
        sender.Send(message);
        isMessageSend = true;
    }

}
